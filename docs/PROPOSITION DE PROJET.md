 # ESQUISSE D'UN ENJEU : LAMPE MODULAIRE EVOLUTIVE ET ADAPTABLE

Contexte de l'idée : 
Avec une année bien chargée et un espace de travail surchargé, l'envie m'est venue de ma nécessité et pour son utilité.
Mon choix s'est arrêté sur une lampe pour plan de travail. Ayant une table d'architecture mais ayant cassé sa lampe d'appoint, je me retrouve dans une situation où la lumière de mon espace était insuffisante et inadéquate. 

| L'enjeu |
| -|
| ![](docs/images/Croquis.jpg) |

### Variante structurelle possible (Structure mobile sur roulette) - Avec une colonne cylindrique en rotation.


![](docs/images/croquisi.png) 

## Référence et inspiration

![](docs/images/JOE.jpg) 

Jonathan de Pas et son Brick System est la référence Mais je m'inspire aussi du mobilier Haller de USM qui à été revisité par la Middle East Architecture Network, avec son projet CO-CROSS.

![](docs/images/MEAN.jpg) 


## Evolution 

V0 

 Constat : l'accroche est inadaptée

 -> On redimmensionne et on modifie la morphologie de l'élément.

V1

Constat : La jonction est **trop petite** - l'accroche ne tient pas, elle se **casse**.

-> Il faut renforcer la densité et la taille des éléments en saillis.

V2 

Constat : Embout toujours **trop petit**. 

-> On va **l'agrandir**

V3

Constat : L'embout est désormais **trop gros**.

-> On va **allonger** l'encastrement de la brique et rajouter une plateforme de **support d'impression**.

V4

Constat : L'embout est à présent **adéquat**.

-> Mise en place des barres LED

V5 

Constat : Le contact unique entre les bande LED ne sont pas fiable.

-> On va donc extruder des extrémités pour que l'on puisse rajouter des **billes de soudure**.

V6 

Constat : La connexion est toujours **difficile**.

-> On va rajouter des **aimants**.

V7 et V8 - Finition sur les détails.

### La jonction valide, et le Test LED

Le principe de ma lampe est que le contact se fait en glissant deux jonctions dans les encastrements prévus à cet effet.

| Le principe | Jonction LED difficile au contact direct sans soudure
| -| - |
| ![](docs/images/Jonction1.gif) | ![](docs/images/Test_LED.jpg) |

# PROTOTYPAGE

### Variante morphologique de la structure (Sketchup)

|  |  |  | |
| --- | --- |---|---|
| ![](docs/images/v1.png) | ![](docs/images/v2.png) | ![](docs/images/v3.png) | ![](docs/images/v4.png) |

# REALISATION DU PROTOTYPE FONCTIONNEL PAR IMPRESSION 3D

## LES MODULES

Type de matériaux de filament utilisé :
Plastique PLA Blanc perle 1.75       
Plastique PLA Gris  métal 1.75

| Les différents modules | Légende |
| --- | --- |
| ![](docs/images/tout_module_unique.jpg) | **1.** Jonction F **2.** Briques pour led **3.** Jonction M **4.** Quadri-jonction **5.** Jonction verticale **6.** Cache led de finition **7.** Colonne portante **8.** Pied-pince **9.** Pied-plat

**Ensemble des pièces**

![](docs/images/A.jpg)



| N°|  Description                   |  Quantité*  |  Notes                                   |        |
|---|--------------------------------|-------------|------------------------------------------|--------|
| 1 | Pied en pince                  |     1       |  Système s'accroche au table             |  ![](docs/images/Pied_pince.jpg)      |
| 2 | Pied plat                      |     1       |  Pour surface plate                      |  ![](docs/images/PIED_PLAT.jpg)      |
| 3 | Module vertical                |     3       |  Colonne portante                        |  ![](docs/images/Colonne.jpg)      |
| 4 | Module vertical et boitier     |     1       |  Colonne avec porte-boitier              |  ![](docs/images/BOITE.jpg)      |
| 5 | Module de jonction verticale   |     4       |  Lien entre deux colonnes                |  ![](docs/images/Colonne.jpg)      |
| 6 | Module quadri-jonction         |     2       |  Lien entre colonne et module horizontal |  ![](docs/images/quadri.jpg)      |
| 7 | Jonction horizontale M         |     3       |  Lien d'entrée du Module horizon.        |  ![](docs/images/Jonction_Table1.jpg)      |
| 8 | Jonction horizontale F         |     3       |  Lien de sortie du Module horizon.       |  ![](docs/images/Jonction_Table.jpg)    |
| 9 | Module horizontal              |     3       |  Brique accueillant la Led               |  ![](docs/images/brique.jpg)      |
| 10| Bande LED divisable            |     1       |  Le luminaire [A trouver ici](https://www.amazon.fr/s?k=LED&__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss_2)                                  |   ![](docs/images/LED.jpg)     |
| 11| Bande cache lumière            |     1       |   Simple finition                        |   ![](docs/images/VITRE.jpg)     |

*_Quantité requise pour l'assemblage démo_

La liste est évidemment non-exhaustive.

## Montage 

| Branchement jonction | Test contact |  | | Le contact fonctionne mais doit être très précis |
| --- | --- | --- | --- | --- |
| ![](docs/images/Montre-brique-jonction.gif) | ![](docs/images/Montre-brique-jonction1.gif) | ![](docs/images/Montre-brique-jonction2.gif) | ![](docs/images/Montre-brique-jonction3.gif) | ![](docs/images/Montre-brique-jonction4.gif) |

| Montage final |  |
| --- | --- |
| ![](docs/images/Branchement1.gif) | ![](docs/images/Branchement2.gif) |

# Mise en situation

| Problème de connexion | Re-soudage des billes | Eureka ça fonctionne |
| --- | --- | --- |
| ![](docs/images/Oops_red_light.jpg) | ![](docs/images/Oops_red_light_2.jpg) | ![](docs/images/Oops_red_light_3.jpg) |
| On remarque qu'il y a un soucis de connexion | Je refais la connexion pour que les billes soient plus grosses | Les connexions semblent adéquates |

| Effet lumière noire | Plan de travail technique |
| --- | --- |
| ![](docs/images/Fin1.jpg) | ![](docs/images/Fin2.jpg) |


