# 4. MODULE.4 - 22/10/2020 - Découpe assistée par ordinateur


## Introduction à la découpe laser : le Lasersaur

Les premières choses à savoir sont les règles de sécurité en utilisant la découpeuse laser.
Lorsque l'on compte utiliser celle-ci, bien s'assurer d'avoir: <ul>ouvert la vanne d'air comprimée <ul>activer l'extracteur de fumée, <ul>mettre en route le refroidisseur à eau <ul>s'assurer que le bouton STATUT soit vert dans Driveboard App, le logiciel d'interface et de gestion de la découpeuse laser.

## Exercice première session

**a) Esquisse ** 

A l'origine, l'idée était de faire une lampe-maquette d'une ville en Karigami.

|  |  |  | |
| --- | --- |---|---|
| ![](docs/images/Karigami1.jpg) | ![](docs/images/Karigami2.jpg) | ![](docs/images/Karigami3.jpg) | ![](docs/images/Karigami4.jpg) |



Malgré le fait que je sache utiliser une découpeuse laser,
Je n'ai pas pu faire ce module par moi même ayant eu des inconvéniants.

