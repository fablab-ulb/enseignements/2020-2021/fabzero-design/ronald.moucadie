# 3. MODULE.3 - 15/10/2020 - Impression 3D


## Au préalable

Il faut tout d'abord exporter le fichier de Fusion360 et l'importer sur le logiciel **Creality Slicer**.
Pour cela il faut l'exporter en **STL**, afin que le Slicer puisse le lire. 
Tout d'abord, on exporte en STL l'objet en question, il faut donc que cet élément soit considéré comme un **objet**.
Notons que nous avons uniquement deux éléments uniques dans ce projet, ce que nous pourrons appeler la brique et la planche pour les distinguer. 

|  Etape 1 | Etape 2 | Etape 3 | Etape 4 |
| --- | --- | --- | --- |
| ![](docs/images/Impre1.JPG) | ![](docs/images/Impre2.JPG) | ![](docs/images/Impre2bis.JPG) | ![](docs/images/Impre3.JPG) |
| Ouvrons d'abord les éléments sur Fusion 360 | Nous exportons en STL chacun des éléments | Cela peut prendre quelques minutes | Enfin, nous avons une icone STL avec un pictogramme sur votre bureau |


Lorsque les exportations sont effectuées, on peut lancer le logiciel d'impression 3D.



## Bienvenue sur Creality Slicer !

### Configuration Creality

Plaçons-les sur la plateforme virtuelle de l'imprimante 3D.

| Etape 1 | Etape 2 | Etape 3 | Etape 4 |
| --- | --- | --- | --- |
| ![](docs/images/00.JPG) | ![](docs/images/01.png) | ![](docs/images/02.JPG) | ![](docs/images/03.JPG) |
| Voici le support virtuel Creality. | Une fois dans le logiciel, on importe nos deux objets STL. | Plaçons-les sur la plateforme virtuelle de l'imprimante 3D en y glissant les fichiers voulus. | Si l'on maximise l'espace pour on peut tout effectuer d'un coup, mais cela sera plus long et cela est plus risqué si il y a un problème durant l'impression. Cela impliquerai de tout recommencer à zéro. Je vais donc effectuer les pièces de façon individuelle ou en petit nombre sans éxcéder 3 Pièces pour cette impression |

Pour cette pièce je n'ai pas besoin de **support** particulier, étant donné que l'essentiel est en **contact** avec le plateau.
Etant donné les petits orifices d'accroche j'ai privilégié une impression lente et précise.




## Test d'Impression 3D du Semestre 1

Premiers tests avec la PrusaSlicer

|||||
| ------------------------ | ------------------------ | ------------------------ | ------------------------ |
| ![](docs/images/3d1.JPG) | ![](docs/images/3d2.JPG) | ![](docs/images/3d3.JPG) | ![](docs/images/3d4.JPG) |
| ![](docs/images/3d5.JPG) | ![](docs/images/3d6.JPG) | ![](docs/images/3d7.JPG) | ![](docs/images/3d8.JPG) |

L'année a été pour moi une véritable épreuve, relativement impossible pour moi de me déplacer à certains moment la question de ma disponibilité concernant la surveillance des impressions fut un problème. Les découvertes des impressions ratées me dépita et me poussa à stopper mon unité d'enseignement pour me consacrer au projet de Mutation. C'est pour cela que j'ai commencé avec PrusaSlicer des Casernes.  


## Seconde session et premiers tests 

Je me suis procurer une imprimante d'entrée de gamme peu cher mais très efficace afin de pouvoir produire un prototype de ma réflexion.
Comme on l'a vu plus haut, il s'agit d'une Creality Ender. 

En me familiarisant avec, j'ai pu effectuer de nombreux tests et prototype téléchargé afin d'être plus à l'aise dans la configuration du plateau.

| Serre-joint - PLA métal | T° 200° et 60° 60mm/s |
|-|-|
| ![](docs/images/04.jpg) | ![](docs/images/05.jpg)|
| Cache-cable - PLA métal | T° 200° et 60° 50mm/s | 
| ![](docs/images/06.jpg) | ![](docs/images/07.jpg)|

# BRICK SYSTEM (Jonathan de Pas)

### Alert Wraping : 
Le **Wraping** est lorsque la base de l'impression accolée au plateau se **décolle**. 
Il est dû en général à une température de plateau **inadéquate**, ou que le **nivellage** du plateau est mal réglé et que la buse est trop **éloignée**. 

| Les wrapings dès le début | Wraping non remarqué | Les coins décollés mais objet fonctionnel|
|-|-|-|
| ![](docs/images/A-00.jpg) | ![](docs/images/A-000.jpg) | ![](docs/images/A-0.jpg)|

## BRICK SYSTEM Final

||||
|-|-|-|
| ![](docs/images/A-1.jpg) | ![](docs/images/A-2.jpg) | ![](docs/images/A-3.jpg)|

