# 1. MODULE.1 - 8/10/2020 - Etat de l'art et documentation

Nous sommes dans les années 70, en plein boom technologique en partie permis par la compétition acharnée entre les USA et l'URSS dans un contexte de guerre froide. L'innovation technique et technologique est très vite mise au profit du design, et les artistes s'en donnent à coeur joie pour imaginer le mobilier de demain. 
Le Brick System, conçu par l'artiste italien Jonathan de Pas, est une structure modulaire composé uniquement de deux modules uniques en plastique moulé. les deux éléments, dupliqués le nombre de fois souhaité, peuvent s'assembler avec un grand nombre de configurations différentes.
Conçu pour être un module très aisement reproduisable en grand nombre sans avoir à modifier les éléments. Il permet d'avoir un ratio coût/rendement intéressant. C'est du design fonctionnaliste à l'état pur, et, esthétiquement parlant, seule la couleur des modules peut apporter une originalité, sans compter la diversité de configuration.

![](docs/images/donato-durbino-and-paolo-lomazzi_.jpg) ![](docs/images/IMG_52525.jpg)

Petit rappel:

Il s'agit du **BRICK SYSTEM**<ul>Conçu en **1970** <ul>Par **Jonathan de Pas** (Designer italien, 1932 - 1991) <ul>En collaboration entre **Donato d'Urbino** and **Paolo Lomazzi** <ul>Technique originel : **plastique ABS moulé**

## Possibilité de réinterprétation

Le système modulaire n'est pas nouveau, c'est un concept très ancien déjà présent dans l'irrigation durant l'Antiquité. Aujourd'hui, la Station Spatiale Internationale montre l'utilité non négligeable de la modularité en mettant clairement en avant ses avantages : plus facile à la conception, plus polyvalent, plus adaptable. 

![](docs/images/Schema_reinterpretation_1.jpg)

Le fait est qu'à cette échelle et avec les contraintes qu'elle suscite, la possibilité d'y ajouter quelque chose semble, tel des légos, un jeu d'enfant. 
Ma première pensée serait de résoudre ce qui, selon moi, son principal problème: son esthétique banal. Difficile de s'approprier de manière harmonieuse un objet en lui donnant une âme, autre qu'un mobilier tout-en-un dont la seule tâche est d'être une planche.

**Comment travailler la modularité, en gardant son aspect pratique et tout en donnant la possibilité de la personnaliser de manière tout aussi facile?** 



## Apprentissage des bases de GitLab

Premier cours, première formation: Gitlab, platforme de style wiki permettant directement d'interagir avec le rendu et d'autre membre de Gitlab.
Chacun partage son travail, son avancé, et tout le monde a accès aux données des autres étudiants permettant ainsi une diffusion des connaissances et des points de vues. 
Le site web ne peux excéder 100Mb, les éléments ajoutés dans le dossier doivent être méticuleusement contrôlés afin d'éviter toute surcharge de la capacité de stockage du cloud.
Le principe est tout simple: éditer les codes de mes fichiers .md afin de documenter sur mon avancé et mon processus et ainsi les partager avec les autres membres du groupe. <ul>J'ai eu beaucoup de mal à me lancer dessus. Le logiciel n'est pas si intuitif lorsque l'on a une vue d'ensemble, et ce n'est que lorsque j'ai compris que la moitié des boutons disponibles ne nous étaient pas utiles, que le logiciel me paru beaucoup plus simple d'utilisation.
J'ai pu aussi relevé certains bug comme par exemple la commande "< ul >", qui ne semble ne fonctionner qu'une fois sur deux et de manière chaotique. 
<ul>

## Apprentissage des bases de Fusion 360

Le volume test de la formation Fusion 360 est **"Le cube Fabzero"**

1) L'apprentissage des bases de 360 fut assez complète et efficace. Tout d'abord on apprend l'outil **Creat Sketch**, on dessine sur un plan la forme voulue. Il a été notifié de bien imaginer la conception de l'objet sur Fusion pendant l'esquisse du projet: que le processus de fabrication soit un critère dans la conception du projet. La face du carré 2D est ensuite **extrudé** pour former le volume (image 1) <ul>
2) Les arêtes sont ensuite sélectionnées puis l'outil **Fillet** est utilisé pour arrondir les bords. (image 2) <ul>
3) Un nouveau sketch est dessiné, et le cercle est extrudé pour former des ouvertures dans le cube et dupliquer l'action avec la commande **Pattern** (image 3 4 5)
4) On créé un rectangle sur une des faces du cube, on extrude une nouvelle fois afin de créer une jonction entre deux ouvertures. On répète ensuite l'action sur les autre arêtes. (image 6 7 8)
5) On finit avec une petite initiation aux apparences avec l'application d'une texture et la prévisualisation de l'objet en Render. (image centrale) <ul>
 

![](docs/images/CUBE_FABZERO.jpg)

