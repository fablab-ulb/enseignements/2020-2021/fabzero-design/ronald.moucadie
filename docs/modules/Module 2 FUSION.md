# 2. MODULE.2 - 15/10/2020 - Conception par ordinateur

Elaboration de la bibliotèque modulaire **BRICK SYSTEM** avec Fusion 360.

La conception de l'objet fut assez simple. La seule réelle difficulté rencontrée est le fait de bien se rappeler la **gestion des calques** pour pouvoir avoir des composants bien fabriqué et que l'on puisse aisement les **modifier**, leur faire effectuer une **translation** etc.

## Création du module structurel

Tout d'abord on **créé le sketch** du bloc principal de la brique (1) puis celui-ci est **extrudé** (2).
Ensuite, des **cercles sont dessinés** sur la face supérieure (3), puis une fois de plus **extrudés** afin de former les chevilles (4) qui formeront les accroches pour l'empilement des briques.<ul>

![](docs/images/Tuto1.jpg)

De la même manière que sur la face supérieure, on **trace des cercles** sur la partie inférieure (5) afin d'y **extruder** les trous permettant d'encastrer les chevilles (6). Notre première brique presque finie (7), on va venir rajouter les accroches de stabilisation supposées (8)

![](docs/images/Tuto2.jpg)

Après un **Sketch** puis une **Extrusion**, on vient posé l'outil **chanfrein** sur les arêtes supérieures afin d'affiner l'accroche (9) et on **Copie/Colle/Move** afin de le dupliquer et ajouter la deuxième accroche (10). Puis, sur la partie inférieure, on **trace** la forme où les extrusions chanfreinées viendront s'encastrer (11), puis de la même façon, on **extrude** à la profondeur adéquate (12). 

![](docs/images/Tuto3.jpg)

## Création de l'étagère et montage de l'ensemble

A présent, les briques sont dupliquées à convenance (13 14), puis certaines d'entre-elles sont fusionnées avec **extrusion** en reliant les deux briques (15 16).

![](docs/images/Tuto4.jpg)

Les briques maintenant terminées, les accroches chanfreinées et la moitié des chevilles de la planche sont retirées (17 18). Puis, la planche maintenant terminée, on peut assembler sa bibliotèque modulaire à sa convenance (19 20).  

![](docs/images/Tuto5.jpg)

## Mise en texture de base

![](docs/images/texturetest_fusion360.png)

## Résultat en render

![](docs/images/render_test.jpg)

