## INTRODUCTION à l'Option ARCHI & DESIGN - Qui suis-je ?

Salut à toi, je te souhaite la **bienvenue** sur ma page !

![](docs/images/Photo_Ro.jpg)

Je me nomme **Ronald Moucadié**, <ul> J'ai 28 ans et je viens de Courbevoie, en banlieue parisienne.
Etudiant en **master** d'architecture à l'ULB, ce cours signifie pour moi l'action de combler mes lacunes en technologie de fabrication.
Je suis né de parents Libanais d'origine grec, s'étant rencontrer à Paris, en ayant fuit le Liban avec la diaspora durant la guerre civile de 1975. Cette diversité d'origine m'a permis de baigner dans un **univers multiculturelle** et démultiplier mes points de vue sur le monde qui m'entoure. Ayant grandi à la Défense, près de Paris, j'ai développé un sens aiguisé de la **perception visuelle** du vide et du plein dans un espace tridimentionnel. Ces découvertes quotidiennes de nature spatiale sont venues enrichir des pensées déjà centrées sur **l'espace**. 

J'espère arriver à vous faire partager ma méthode, ma perception, ma sensibilité à travers ce devoir.
En vous souhaitant **une bonne lecture !** 


## Parcours antérieur

Je suis né à Livry-Gargan près de Paris, mais je ne connais absolument pas l'endroit. Nous habitions à Courbevoie dans le quartier de la Défense, dans les Hauts-de-Seine. J'ai été scolarisé à Notre-Dame de Sainte-Croix de Neuilly jusqu'en 2nde puis à Passy-Saint-Honoré pour mes deux dernières années de Lycée et mon baccalauréat en Sciences et Technologie de la Gestion et Marketing (STG). En 2011, j'intègre l'Académie Charpentier puis l'Atelier de Sèvre l'année suivante. En 2013, je rejoins l'ULB en architecture, 1 an avant la mise en place du décret Paysage, dit Marcourt. J'essaye, durant mes années, de m'épanouir un maximum quitte à ce que, parfois, cela puisse  m'être préjudiciable. En 2018 je pars en Chine, de Honkong à Péking, avec moyen d'expression dirigé par Kiran Katara. Puis, en 2019, je pars à Détroit avec l'atelier Micromegas, supervisé par Eve Deprez et Alain Simon. En 2019-2020, j'effectue un echange universitaire avec l'ETSAB de l'UPC, à Barcelone. Durant cette période, j'ai pu mieux capter le mode de vie méditerrannéen occidental. Cela m'a aussi permis d'effectuer un voyage d'étude sur la légendaire Troie et pour lequel nous sommes allés en Grèce et en Turquie. Revenu à Bruxelles pour une dernière année, je profite pour combler mes carences en conception de projet.


## Ancien travaux

Je suis un inconditionnel de Sketchup. Logiciel appris dès ma première année, il est omniprésent dans l'élaboration de mes projets : que ce soit pour les dimensions et les superficies, pour une maquette complète ou tout simplement des schémas, Sketchup est mon outils de prédilection, bien qu'il tend à s'effacer derrière d'autres logiciels plus performant et professionnel. Doublé de cela, je fais plutôt du travail à la main soit des productions hybrides (Dessin Ipad, sur impression etc.)

![Photo à insérer](docs/images/Sans_titre-1.jpg)



### Projet d'Architecture et Design (Q1-20-21)

Oeuvre design du BDM retenue : <ul>
Il s'agit du **BRICK SYSTEM**<ul>Conçu en **1970** <ul>Par **Jonathan de Pas** (Designer italien, 1932 - 1991) <ul>En collaboration entre **Donato d'Urbino** and **Paolo Lomazzi** <ul>Technique originel : **plastique ABS moulé**

Modulaire et duplicable, j'apprécie le fait de toucher facilement à la modification et cela au point d'obtenir un résultat dont la différence est notable et qu'elle permet une redécouverte de l'objet, une nouvelle nature.

![Photo à insérer](docs/images/Capture2.jpg) ![ Image à insérer ](docs/images/IMG_52525.jpg)

